<?php
use Illuminate\Http\Request;
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('foo', function () {
    return 'Hello World';
});


$router->group(['prefix'=>'employee'],function() use ($router){
    $router->get('/',['uses'=>'EmployeeController@show','as'=>'show']);
    $router->get('/create',['uses'=>'EmployeeController@create','as'=>'create']);
    $router->post('/store',['uses'=>'EmployeeController@store','as'=>'store']);
});

$router->group(['prefix'=>'api/auth','middleware'=>'api'],function () use ($router){
    $router->post('/login',['uses'=>'AuthController@login']);
    $router->post('/register',['uses'=>'AuthController@register']);



});


