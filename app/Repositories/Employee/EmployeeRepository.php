<?php
namespace App\Repositories\Employee;

use App\Repositories\BaseRepository;

class EmployeeRepository extends BaseRepository implements EmployeeRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\Employee::class;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function getProduct()
    {
        return $this->model->select('name')->take(2)->get();
    }
}
