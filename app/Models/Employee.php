<?php
namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model;

class Employee extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'employee';
    protected $fillable = ['name','email'];

}

