<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Repositories\Employee\EmployeeRepository;
use App\Repositories\Employee\EmployeeRepositoryInterface;
use Illuminate\Http\Request;


class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $employeeRepo;
    public function __construct(
        EmployeeRepositoryInterface $employeeRepo
    )
    {
        $this->employeeRepo=$employeeRepo;
    }
    public function show(){
        $employee=$this->employeeRepo->all();
        return response()->json($employee);
    }
    public function create(){

        return view('create');
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users'
        ]);
        Employee::create(['name' => $request->input('name'),'email' => $request->input('email')]);
        return response()->json(['message'=>'success'],200);
    }
    //
}
